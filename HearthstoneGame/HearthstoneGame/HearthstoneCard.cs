﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace HearthstoneGame
{
    public class HearthstoneCard
    {
        public string _name { get; set; }
        public string _description { get; set; }
        public int _cost { get; set; }

        public HearthstoneCard(int cost, string description, string name)
        {
            this._cost = cost;
            this._description = description;
            this._name = name;
        }

        public HearthstoneCard()
        {
            _cost = 0;
            _description = string.Empty;
            _name = string.Empty;
        }

        public static List<HearthstoneCard> createDeck()
        {
            var deck = new List<HearthstoneCard>();

            for (int i = 0; i < 10; i++)
            {
                var card = new HearthstoneCard();
                deck.Add(card);
            }
            
            return deck;
            
        }
        
        public override string ToString()
        {
            var str = new StringBuilder();
            str.AppendLine($"Name is: {_name}");
            str.AppendLine($"Description is: {_description}");
            str.AppendLine($"Cost is: {_cost}");
            
            return str.ToString();
        }

        public static string Serialize(HearthstoneCard card)
        {
            var json = JsonSerializer.Serialize(card);
            return json;
        }

        public static string prettySerialize(HearthstoneCard card)
        {
            var options = new JsonSerializerOptions
            {
                WriteIndented = true,
            };

            var json = JsonSerializer.Serialize(card, options);
            return json;
        }
    }
}

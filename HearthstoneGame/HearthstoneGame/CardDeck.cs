﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace HearthstoneGame
{
    public class CardDeck
    {
        public List<HearthstoneCard> deck { get; set; }
        public CardDeck(int numOfCards)
        {
            deck = new List<HearthstoneCard>(numOfCards);
            
            for (int i = 0; i < numOfCards; i++)
            {
                deck.Add(new HearthstoneCard());
                var card = deck.ElementAt(i);

                card._name = ($"Card {i+1}");
            }
        }

        public void saveDeckToJson(string filePath)
        {
            string path = $"C:\\tmp\\{filePath}.json";

            if (!File.Exists(path))
            {
                StreamWriter sw = File.CreateText(path);
                
                sw.WriteLine(JsonSerializer.Serialize(deck));
                sw.Close();
            }
        }

        public void loadDeckFromFile(string name)
        {
            string path = $"C:\\tmp\\{name}.txt";

            if (File.Exists(path))
            {
                StreamReader sr = File.OpenText(path);

                string cardName = sr.ReadLine();
                
                HearthstoneCard newCard = new HearthstoneCard(0, "Test card", cardName);
                Console.WriteLine(newCard.ToString());
                
                sr.Close();
            }
        }
        
    }
}
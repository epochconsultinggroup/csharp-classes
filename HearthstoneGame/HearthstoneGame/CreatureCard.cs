﻿using System;
using System.Diagnostics;
using System.Text;

namespace HearthstoneGame
{
    public class CreatureCard : HearthstoneCard
    {
        private int _attackValue;
        private int _health;

        public CreatureCard(int attackValue, int health, int cost, string description, string name)
        {
            _attackValue = attackValue;
            _health = health;
            _cost = cost;
            _description = description;
            _name = name;

        }

        public CreatureCard()
        {
            _attackValue = 0;
            _health = 0;
            _cost = 0;
            _description = string.Empty;
            _name = string.Empty;


        }

        public void Attack(CreatureCard cardToAttack)
        {
            cardToAttack._health -= this._attackValue;

            if (cardToAttack._health <= 0)
            {
                Console.WriteLine("The " + cardToAttack._name + " has perished.");
            }
        }

        public int getAttackValue()
        {
            return this._attackValue;
        }

        public void setAttackValue(int newValue)
        {
            this._attackValue = newValue;
        }

        public int getHealth()
        {
            return this._health;
        }

        public void setHealth(int newValue)
        {
            this._health = newValue;
        }

        public override string ToString()
        {
            var str = new StringBuilder();

            str.AppendLine($"Name is: {_name}");

            return str.ToString();
        }
    }
}

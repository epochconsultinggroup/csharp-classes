﻿using System;
using System.Text.Json;
using System.Text.Json.Serialization;
using Microsoft.VisualBasic.CompilerServices;

namespace HearthstoneGame
{
    internal class Program
    {
        private static void Main()
        {
            
            var testcard = new HearthstoneCard(0, "Test Description", "Sample Card");
            
            var ratKing = new CreatureCard(5, 10, 1, "The almighty king of rats", "Rat King");
            
            var ratPeon = new CreatureCard(1, 5, 1, "The almighty peon of rats", "Rat Peon");
            
            ratKing.Attack(ratPeon);

            CardDeck deck = new CardDeck(10);

            string jsonString = HearthstoneCard.Serialize(ratKing);
            Console.WriteLine(jsonString);

            string prettyJsonString = HearthstoneCard.prettySerialize(ratKing);
            Console.WriteLine(prettyJsonString);

            deck.saveDeckToJson("test");

        }
    }
}

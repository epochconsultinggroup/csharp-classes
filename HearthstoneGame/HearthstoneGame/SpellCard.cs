﻿using System;
using System.Text;

namespace HearthstoneGame
{
    public class SpellCard : HearthstoneCard
    {

        public SpellCard(int cost, string description, string name)
        {
            _cost = cost;
            _description = description;
            _name = name;
        }

        public SpellCard()
        {
            _cost = 0;
            _description = string.Empty;
            _name = string.Empty;
        }
    }
}
using HearthstoneGame;
using NUnit.Framework;

namespace UnitTests
{
    public class CreatureCardTests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void AttackCardKillsCreature()
        {
            var attackingCard = new CreatureCard(5, 10, 1, "This card has high health and damage", "Rat King");
            var defendingCard = new CreatureCard(2, 5, 1,"This card has low health and damage", "Rat Peon");

            attackingCard.Attack(defendingCard);
            
            Assert.IsTrue(defendingCard.getHealth() == 0);
        }
    }
}